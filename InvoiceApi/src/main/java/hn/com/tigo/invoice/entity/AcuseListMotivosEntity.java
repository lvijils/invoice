package hn.com.tigo.invoice.entity;

import hn.com.tigo.invoice.dto.AcuseAllMotivosDTO;
import hn.com.tigo.josm.persistence.core.EntityBase;
import hn.com.tigo.josm.persistence.core.SessionBase;
import hn.com.tigo.josm.persistence.exception.PersistenceError;
import hn.com.tigo.josm.persistence.exception.PersistenceException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AcuseListMotivosEntity extends EntityBase<AcuseAllMotivosDTO> {

    private final String _coreSelectMotivos;

    public AcuseListMotivosEntity(final SessionBase sessionBase) throws PersistenceException {
        super(sessionBase);
        _coreSelectMotivos = "SELECT id_motivo AS id,nombre_motivo FROM MOTIVOS_ACUSE";
    }

    @Override
    protected List<AcuseAllMotivosDTO> fillList(ResultSet rs) throws PersistenceException {
        AcuseAllMotivosDTO dto;
        ArrayList<AcuseAllMotivosDTO> lista = new ArrayList<AcuseAllMotivosDTO>();
        dto = new AcuseAllMotivosDTO();
        try {
            while (rs.next()) {
                dto = new AcuseAllMotivosDTO();
                dto.setId(rs.getInt("id"));
                dto.setNombre_motivo(rs.getString("nombre_motivo"));
                lista.add(dto);
            }
            return lista;
        } catch (SQLException e) {
            throw new PersistenceException(PersistenceError.SQL, e);
        }

    }

    public final List<AcuseAllMotivosDTO> getAllMotivos() throws PersistenceException {
        return executeSelectStatement(_coreSelectMotivos);
    }
}
