package hn.com.tigo.invoice.entity;

import hn.com.tigo.invoice.dto.AcuseAllPerCiclosDTO;
import hn.com.tigo.josm.persistence.core.EntityBase;
import hn.com.tigo.josm.persistence.core.SessionBase;
import hn.com.tigo.josm.persistence.exception.PersistenceError;
import hn.com.tigo.josm.persistence.exception.PersistenceException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AllPerCiclosEntity extends EntityBase<AcuseAllPerCiclosDTO> {

    private final String _coreSelectAllPerCiclos;

    public AllPerCiclosEntity(final SessionBase sessionBase) throws PersistenceException {
        super(sessionBase);
        _coreSelectAllPerCiclos = "SELECT periodos,ciclos FROM V_PERIODOS_CICLOS";
    }

    @Override
    protected List<AcuseAllPerCiclosDTO> fillList(ResultSet rs) throws PersistenceException {
        AcuseAllPerCiclosDTO dto;
        ArrayList<AcuseAllPerCiclosDTO> lista = new ArrayList<AcuseAllPerCiclosDTO>();
        dto = new AcuseAllPerCiclosDTO();
        try {
            while (rs.next()) {
                dto = new AcuseAllPerCiclosDTO();
                dto.setResPeriodo(rs.getString("periodos"));
                dto.setResCiclos(rs.getString("ciclos"));

                lista.add(dto);
            }
            return lista;
        } catch (SQLException e) {
            throw new PersistenceException(PersistenceError.SQL, e);
        }

    }

    public final List<AcuseAllPerCiclosDTO> getAllPerCiclos() throws PersistenceException {
        return executeSelectStatement(_coreSelectAllPerCiclos);
    }
}
