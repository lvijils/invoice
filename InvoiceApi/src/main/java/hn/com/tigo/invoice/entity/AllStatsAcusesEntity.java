package hn.com.tigo.invoice.entity;

import hn.com.tigo.invoice.dto.AcuseAllStatsDTO;
import hn.com.tigo.josm.persistence.core.EntityBase;
import hn.com.tigo.josm.persistence.core.SessionBase;
import hn.com.tigo.josm.persistence.exception.PersistenceError;
import hn.com.tigo.josm.persistence.exception.PersistenceException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AllStatsAcusesEntity extends EntityBase<AcuseAllStatsDTO> {

    private final String _coreAllStatsAcuses;

    public AllStatsAcusesEntity(final SessionBase sessionBase) throws PersistenceException {
        super(sessionBase);
        _coreAllStatsAcuses = "SELECT COUNT(REGISTRO_ACUSES.ID_MOTIVO) AS COUNT, NOMBRE_MOTIVO FROM REGISTRO_ACUSES JOIN MOTIVOS_ACUSE ON(REGISTRO_ACUSES.ID_MOTIVO=MOTIVOS_ACUSE.ID_MOTIVO) WHERE ID_PERIODO=? AND ID_CICLO=? AND ENTREGADO=0 GROUP BY REGISTRO_ACUSES.ID_MOTIVO,NOMBRE_MOTIVO UNION SELECT SUM(COUNT(REGISTRO_ACUSES.ID_MOTIVO))AS COUNT, '1A' AS NOMBRE_MOTIVO FROM REGISTRO_ACUSES JOIN MOTIVOS_ACUSE ON(REGISTRO_ACUSES.ID_MOTIVO=MOTIVOS_ACUSE.ID_MOTIVO) WHERE ID_PERIODO=? AND ID_CICLO=? AND ENTREGADO=1 GROUP BY REGISTRO_ACUSES.ID_MOTIVO ORDER BY NOMBRE_MOTIVO";
    }

    @Override
    protected List<AcuseAllStatsDTO> fillList(ResultSet rs) throws PersistenceException {
        AcuseAllStatsDTO dto;
        ArrayList<AcuseAllStatsDTO> lista = new ArrayList<AcuseAllStatsDTO>();
        dto = new AcuseAllStatsDTO();
        try {
            while (rs.next()) {
                dto = new AcuseAllStatsDTO();
                dto.setCount(rs.getInt("count"));
                dto.setNombre_motivo(rs.getString("nombre_motivo"));
                //dto.setEntregado(rs.getString("entregado"));
                lista.add(dto);
            }
            return lista;
        } catch (SQLException e) {
            throw new PersistenceException(PersistenceError.SQL, e);
        }

    }

    public final List<AcuseAllStatsDTO> getAllStatasAcuses(final String periodo, final String cycle) throws PersistenceException {
        return executeSelectStatement(_coreAllStatsAcuses, periodo, cycle, periodo, cycle);
    }

}
