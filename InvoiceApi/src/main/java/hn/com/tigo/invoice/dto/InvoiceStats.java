
package hn.com.tigo.invoice.dto;

public class InvoiceStats extends DTO{
    private long count; // Tipo DB: NUMBER    
    private String strerror; // Tipo DB: NUMBER
    private String status; // Tipo DB: CLOB

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public String getStrerror() {
        return strerror;
    }

    public void setStrerror(String strerror) {
        this.strerror = strerror;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    } 
}
