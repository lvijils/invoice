package hn.com.tigo.invoice.entity;

import hn.com.tigo.invoice.dto.InvoiceStats;
import hn.com.tigo.josm.persistence.core.EntityBase;
import hn.com.tigo.josm.persistence.core.SessionBase;
import hn.com.tigo.josm.persistence.exception.PersistenceError;
import hn.com.tigo.josm.persistence.exception.PersistenceException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class InvoiceStatsEntity extends EntityBase<InvoiceStats> {

    private final String _coreSelectStatsCycles;

    public InvoiceStatsEntity(final SessionBase sessionBase) throws PersistenceException {
        super(sessionBase);
        _coreSelectStatsCycles = "SELECT count(A.status) \"COUNT\",A.ERROR,A.status FROM invoice_config.listener_dist_file A INNER JOIN invoice_config.LISTENER_PROCESS_FILE B ON A.ID_PROCESS_FILE=B.ID WHERE B.CYCLE=? group by A.status, B.CYCLE,A.ERROR";
    }

    @Override
    protected final ArrayList<InvoiceStats> fillList(ResultSet result) throws PersistenceException {
        InvoiceStats dto;
        ArrayList<InvoiceStats> lista = new ArrayList<>();
        try {
            while (result.next()) {
                dto = new InvoiceStats();
                dto.setCount(result.getLong("COUNT"));
                dto.setStrerror(result.getString("ERROR"));
                dto.setStatus(result.getString("STATUS"));
                lista.add(dto);
            }
            return lista;
        } catch (SQLException e) {
            throw new PersistenceException(PersistenceError.SQL, e);
        }
    }

    public final List<InvoiceStats> getStatsCycle(final String billCycle) throws PersistenceException {
        return executeSelectStatement(_coreSelectStatsCycles, billCycle);
    }

}
