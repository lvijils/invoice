package hn.com.tigo.invoice.dto;

public class AllStatsDTO extends DTO {

    private String cyclo; // Tipo DB: STRING 

    public String getCyclo() {
        return cyclo;
    }

    public void setCyclo(String cyclo) {
        this.cyclo = cyclo;
    }
}
