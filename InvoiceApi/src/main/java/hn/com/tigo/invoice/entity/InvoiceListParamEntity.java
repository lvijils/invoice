package hn.com.tigo.invoice.entity;

import hn.com.tigo.invoice.dto.InvoiceList;
import hn.com.tigo.josm.persistence.core.EntityBase;
import hn.com.tigo.josm.persistence.core.SessionBase;
import hn.com.tigo.josm.persistence.exception.PersistenceError;
import hn.com.tigo.josm.persistence.exception.PersistenceException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class InvoiceListParamEntity extends EntityBase<InvoiceList> {

    private final String _coreSelectSubscriber;
    private final String _coreSelectCustCode;
    private final String _coreSelectCustId;
    private final String _coreSelectCustCodeAndBillCycle;
    private final String _coreSelectCustIdAndBillCycle;

    public InvoiceListParamEntity(final SessionBase sessionBase) throws PersistenceException {
        super(sessionBase);
        _coreSelectSubscriber = "SELECT uuid,billingcycleid,billingtype,company,custcode,custid,custtype,datetime,emailaddr,exchangerate,idprocess,smsaddr,invoiceid,billmedium,change,created,primaryidentity,prevbalance,billcyclebegin,billcycleend,name,address,subtotal FROM INVOICE_LIST WHERE PRIMARYIDENTITY = ?";
        _coreSelectCustCode = "SELECT uuid,billingcycleid,billingtype,company,custcode,custid,custtype,datetime,emailaddr,exchangerate,idprocess,smsaddr,invoiceid,billmedium,change,created,primaryidentity,prevbalance,billcyclebegin,billcycleend,name,address,subtotal FROM INVOICE_LIST WHERE CUSTCODE = ? AND ROWNUM < 7";
        _coreSelectCustId = "SELECT uuid,billingcycleid,billingtype,company,custcode,custid,custtype,datetime,emailaddr,exchangerate,idprocess,smsaddr,invoiceid,billmedium,change,created,primaryidentity,prevbalance,billcyclebegin,billcycleend,name,address,subtotal FROM INVOICE_LIST WHERE CUSTID = ? AND ROWNUM < 7";
        _coreSelectCustCodeAndBillCycle = "SELECT uuid,billingcycleid,billingtype,company,custcode,custid,custtype,datetime,emailaddr,exchangerate,idprocess,smsaddr,invoiceid,billmedium,change,created,primaryidentity,prevbalance,billcyclebegin,billcycleend,name,address,subtotal FROM INVOICE_LIST WHERE CUSTCODE = ? AND BILLINGCYCLEID = ? AND ROWNUM < 7";
        _coreSelectCustIdAndBillCycle = "SELECT uuid,billingcycleid,billingtype,company,custcode,custid,custtype,datetime,emailaddr,exchangerate,idprocess,smsaddr,invoiceid,billmedium,change,created,primaryidentity,prevbalance,billcyclebegin,billcycleend,name,address,subtotal FROM INVOICE_LIST WHERE CUSTID = ? AND BILLINGCYCLEID = ? AND ROWNUM < 7";
    }

    @Override
    protected final ArrayList<InvoiceList> fillList(ResultSet result) throws PersistenceException {
        InvoiceList dto;
        ArrayList<InvoiceList> lista = new ArrayList<InvoiceList>();
        try {
            while (result.next()) {
                dto = new InvoiceList();
                dto.setUuid(result.getString("uuid"));
                dto.setBillingcycleid(result.getString("billingcycleid"));
                dto.setBillingtype(result.getString("billingtype"));
                dto.setCompany(result.getString("company"));
                dto.setCustcode(result.getString("custcode"));
                dto.setCustid(result.getString("custid"));
                dto.setCusttype(result.getString("custtype"));
                dto.setDatetime(result.getTimestamp("datetime"));
                dto.setEmailaddr(result.getString("emailaddr"));
                dto.setExchangerate(result.getString("exchangerate"));
                dto.setIdprocess(result.getLong("idprocess"));
                dto.setSmsaddr(result.getString("smsaddr"));
                dto.setInvoiceid(result.getString("invoiceid"));
                dto.setBillmedium(result.getString("billmedium"));
                dto.setCharge(result.getString("change"));
                dto.setCreated(result.getTimestamp("created"));
                dto.setPrimaryidentity(result.getString("primaryidentity"));
                dto.setPrevbalance(result.getString("prevbalance"));
                dto.setBillCycleBegin(result.getString("billcyclebegin"));
                dto.setBillCyleEnd(result.getString("billcycleend"));
                dto.setName(result.getString("name"));
                dto.setAddress(result.getString("address"));
                dto.setSubTotal(result.getString("subtotal"));
                lista.add(dto);
            }
            return lista;
        } catch (SQLException e) {
            throw new PersistenceException(PersistenceError.SQL, e);
        }
    }

    public final List<InvoiceList> getInvoiceSubscriber(final String subscriber, final String current) throws PersistenceException {
        String sql = _coreSelectSubscriber;
        if ("1".equals(current)) {
            sql = sql + " AND ROWNUM < 2";
        } else {
            sql = sql + " AND ROWNUM < 7";
        }
        return executeSelectStatement(sql, subscriber);
    }

    public final List<InvoiceList> getInvoiceCustCode(final String custCode) throws PersistenceException {
        return executeSelectStatement(_coreSelectCustCode, custCode);
    }

    public final List<InvoiceList> getInvoiceCustId(final String custId) throws PersistenceException {
        return executeSelectStatement(_coreSelectCustId, custId);
    }

    public final List<InvoiceList> getInvoiceCustCodeAndBillCycle(final String custCode, final String billCycle) throws PersistenceException {
        return executeSelectStatement(_coreSelectCustCodeAndBillCycle, custCode, billCycle);
    }

    public final List<InvoiceList> getInvoiceCustIdAndBillCycle(final String custId, final String billCycle) throws PersistenceException {
        return executeSelectStatement(_coreSelectCustIdAndBillCycle, custId, billCycle);
    }
}
