package hn.com.tigo.invoice.dto;

public class AcuseAllStatsDTO extends DTO {

    private long count;
    private String nombre_motivo;
    private String entregado;

    public String getEntregado() {
        return entregado;
    }

    public void setEntregado(String entregado) {
        this.entregado = entregado;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public String getNombre_motivo() {
        return nombre_motivo;
    }

    public void setNombre_motivo(String nombre_motivo) {
        this.nombre_motivo = nombre_motivo;
    }
}
