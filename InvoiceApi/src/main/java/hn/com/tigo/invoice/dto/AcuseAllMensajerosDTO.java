package hn.com.tigo.invoice.dto;

public class AcuseAllMensajerosDTO extends DTO {

    private int id;
    private String nombre_mensajero;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre_mensajero() {
        return nombre_mensajero;
    }

    public void setNombre_mensajero(String nombre_mensajero) {
        this.nombre_mensajero = nombre_mensajero;
    }
}
