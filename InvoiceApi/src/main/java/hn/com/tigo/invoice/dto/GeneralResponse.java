package hn.com.tigo.invoice.dto;

import java.util.List;

/**
 *
 * @author Leovijil
 */
public class GeneralResponse {

    private String code;
    private String description;
    private List<InvoiceList> response;
    private List<InvoiceStats> responseStats;
    private List<AllStatsDTO> responseAllStats;
    private List<InvoiceStatusModel> responseStatus;
    private List<AcuseAllPerCiclosDTO> responseAll;
    private List<AcuseAllMensajerosDTO> responseMensajeros;
    private List<AcuseAllMotivosDTO> responseMotivos;
    private List<AcuseAllStatsDTO> responseStatsAcuses;
    private List<AllstatsMensByPerCicloDTO> responseStatsMensajeros;
    private List<AcuseListDTO> responseAcuses;
    private List<AcuseBycicloPerClienteDTO> existe;
    private List<AcuseRowDTO> fullRowAcuses;

    public List<AcuseRowDTO> getFullRowAcuses() {
        return fullRowAcuses;
    }

    public void setFullRowAcuses(List<AcuseRowDTO> fullRowAcuses) {
        this.fullRowAcuses = fullRowAcuses;
    }

    public List<AcuseBycicloPerClienteDTO> getExiste() {
        return existe;
    }

    public void setExiste(List<AcuseBycicloPerClienteDTO> existe) {
        this.existe = existe;
    }

    public List<AcuseListDTO> getResponseAcuses() {
        return responseAcuses;
    }

    public void setResponseAcuses(List<AcuseListDTO> responseAcuses) {
        this.responseAcuses = responseAcuses;
    }

    public List<AllstatsMensByPerCicloDTO> getResponseStatsMensajeros() {
        return responseStatsMensajeros;
    }

    public void setResponseStatsMensajeros(List<AllstatsMensByPerCicloDTO> responseStatsMensajeros) {
        this.responseStatsMensajeros = responseStatsMensajeros;
    }

    public List<AcuseAllStatsDTO> getResponseStatsAcuses() {
        return responseStatsAcuses;
    }

    public void setResponseStatsAcuses(List<AcuseAllStatsDTO> responseStatsAcuses) {
        this.responseStatsAcuses = responseStatsAcuses;
    }

    public List<AcuseAllMotivosDTO> getResponseMotivos() {
        return responseMotivos;
    }

    public void setResponseMotivos(List<AcuseAllMotivosDTO> responseMotivos) {
        this.responseMotivos = responseMotivos;
    }

    public List<AcuseAllPerCiclosDTO> getResponseAll() {
        return responseAll;
    }

    public void setResponseAll(List<AcuseAllPerCiclosDTO> responseAll) {
        this.responseAll = responseAll;
    }

    public List<AcuseAllMensajerosDTO> getResponseMensajeros() {
        return responseMensajeros;
    }

    public void setResponseMensajeros(List<AcuseAllMensajerosDTO> responseMensajeros) {
        this.responseMensajeros = responseMensajeros;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<InvoiceStatusModel> getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(List<InvoiceStatusModel> responseStatus) {
        this.responseStatus = responseStatus;
    }

    public List<InvoiceStats> getResponseStats() {
        return responseStats;
    }

    public void setResponseStats(List<InvoiceStats> responseStats) {
        this.responseStats = responseStats;
    }

    public List<AllStatsDTO> getResponseAllStats() {
        return responseAllStats;
    }

    public void setResponseAllStats(List<AllStatsDTO> responseAllStats) {
        this.responseAllStats = responseAllStats;
    }

    public List<AcuseAllPerCiclosDTO> getResponseAllPerCiclos() {
        return responseAll;
    }

    public void setResponseAllPerCiclos(List<AcuseAllPerCiclosDTO> responseAll) {
        this.responseAll = responseAll;
    }

    public List<InvoiceList> getResponse() {
        return response;
    }

    public void setResponse(List<InvoiceList> response) {
        this.response = response;
    }
    
    
}
