package hn.com.tigo.invoice.dto;

public class AcuseAllPerCiclosDTO extends DTO {

    private String resPeriodos;
    private String resCiclos;

    public String getResCiclos() {
        return resCiclos;
    }

    public void setResCiclos(String resCiclos) {
        this.resCiclos = resCiclos;
    }

    public String getResPeriodo() {
        return resPeriodos;
    }

    public void setResPeriodo(String resPeriodo) {
        this.resPeriodos = resPeriodo;
    }
}
