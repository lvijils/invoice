/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hn.com.tigo.invoice;

import hn.com.tigo.invoice.AdapterValidationType;
import com.google.gson.Gson;
import hn.com.tigo.invoice.dto.AcuseBycicloPerClienteDTO;
import hn.com.tigo.invoice.dto.AcuseAllMensajerosDTO;
import hn.com.tigo.invoice.dto.AcuseAllMotivosDTO;
import hn.com.tigo.invoice.dto.AcuseAllPerCiclosDTO;
import hn.com.tigo.invoice.dto.AllStatsDTO;
import hn.com.tigo.invoice.dto.AcuseAllStatsDTO;
import hn.com.tigo.invoice.dto.AllstatsMensByPerCicloDTO;
import hn.com.tigo.invoice.dto.GeneralResponse;
import hn.com.tigo.invoice.dto.InvoiceList;
import hn.com.tigo.invoice.dto.InvoiceStats;
import hn.com.tigo.invoice.dto.InvoiceStatusModel;
import hn.com.tigo.invoice.dto.AcuseListDTO;
import hn.com.tigo.invoice.dto.AcuseRowDTO;
import hn.com.tigo.invoice.manager.InvoiceManager;
import hn.com.tigo.josm.persistence.core.ServiceSessionEJB;
import hn.com.tigo.josm.persistence.core.ServiceSessionEJBLocal;
import hn.com.tigo.josm.persistence.exception.PersistenceException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * REST Web Service
 *
 * @author Leovijil
 */
@Path("invoice")
public class InvoiceApis {

    @Context
    private UriInfo context;
    
    private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceApis.class);

    /**
     * Creates a new instance of GenericResource
     */
    public InvoiceApis() {
    }

    /**
     * Retrieves representation of an instance of hn.com.tigo.invoice.InvoiceApis
     * @param subscriber
     * @return an instance of java.lang.String
     */
    
    @POST
    @Path("/addAcuse")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON) // LO QUE SE MANDA DESDE EL CLIENTE 
    @Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON) // LO QUE MANDO DESDE LA API
    public Response postAddAcuse(final AcuseListDTO request) {
    InvoiceManager manager = null;
        GeneralResponse response = new GeneralResponse();
         try{
            ServiceSessionEJBLocal<InvoiceManager> serviceSession = ServiceSessionEJB.getInstance();
            manager = (InvoiceManager) serviceSession.getSessionDataSource(InvoiceManager.class, "Invoice");
            manager.insertAcuseForm(request);
            response.setCode("0");
            response.setDescription("Transaction Complete!!!");
        }catch(PersistenceException e){
            java.util.logging.Logger.getLogger(InvoiceApis.class.getName()).log(Level.SEVERE, null, e);
        }finally{
            if(manager != null){
                try {
                    manager.close();
                } catch (PersistenceException ex) {
                    java.util.logging.Logger.getLogger(InvoiceApis.class.getName()).log(Level.SEVERE, null, ex);
                }
            }    
        }
         Gson json = new Gson();
        return Response
            .status(200)
            .header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
            .header("Access-Control-Allow-Credentials", "true")
            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
            .header("Access-Control-Max-Age", "1209600")
            .entity(json.toJson(response))
            .build();
    }
    
    
    @POST
    @Path("/updateAcuse")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON) // LO QUE SE MANDA DESDE EL CLIENTE 
    @Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON) // LO QUE MANDO DESDE LA API
    public GeneralResponse postUpAcuse(final AcuseListDTO request) {
    InvoiceManager manager = null;
        GeneralResponse response = new GeneralResponse();
         try{
            ServiceSessionEJBLocal<InvoiceManager> serviceSession = ServiceSessionEJB.getInstance();
            manager = (InvoiceManager) serviceSession.getSessionDataSource(InvoiceManager.class, "Invoice");
            manager.updateAcuseForm(request);
            response.setCode("0");
            response.setDescription("Transaction Complete!!!");
        }catch(PersistenceException e){
            java.util.logging.Logger.getLogger(InvoiceApis.class.getName()).log(Level.SEVERE, null, e);
        }finally{
            if(manager != null){
                try {
                    manager.close();
                } catch (PersistenceException ex) {
                    java.util.logging.Logger.getLogger(InvoiceApis.class.getName()).log(Level.SEVERE, null, ex);
                }
            }    
        }
          return response;
    }
    
    
    @POST
    @Path("/deleteAcuse")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON) // LO QUE SE MANDA DESDE EL CLIENTE 
    @Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON) // LO QUE MANDO DESDE LA API
    public GeneralResponse deleteAcuse(final AcuseListDTO request) {
    InvoiceManager manager = null;
        GeneralResponse response = new GeneralResponse();
         try{
            ServiceSessionEJBLocal<InvoiceManager> serviceSession = ServiceSessionEJB.getInstance();
            manager = (InvoiceManager) serviceSession.getSessionDataSource(InvoiceManager.class, "Invoice");
            manager.deleteAcuseForm(request);
            response.setCode("0");
            response.setDescription("Transaction Complete!!!");
        }catch(PersistenceException e){
            java.util.logging.Logger.getLogger(InvoiceApis.class.getName()).log(Level.SEVERE, null, e);
        }finally{
            if(manager != null){
                try {
                    manager.close();
                } catch (PersistenceException ex) {
                    java.util.logging.Logger.getLogger(InvoiceApis.class.getName()).log(Level.SEVERE, null, ex);
                }
            }    
        }
          return response;
    }
    
    
    @GET
    @Path("/getExisteCliente/{periodo}/{ciclo}/{cliente}/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getExisteCliente(@PathParam("periodo") String periodo,@PathParam("ciclo") String ciclo,@PathParam("cliente") String cliente,@PathParam("id") String id) {
        InvoiceManager manager = null;
        List<AcuseBycicloPerClienteDTO> response = null;
        GeneralResponse generalResponse = new GeneralResponse();
        try{
            ServiceSessionEJBLocal<InvoiceManager> serviceSession = ServiceSessionEJB.getInstance();
            manager = (InvoiceManager) serviceSession.getSessionDataSource(InvoiceManager.class, "Invoice");
            response = manager.getExisteCliente(periodo,ciclo,cliente,id);
            if(response.isEmpty()){
                generalResponse.setCode("100");
                generalResponse.setDescription("No se encontraron datos.");
                generalResponse.setExiste(response);
            }else{
                generalResponse.setCode("0");
                generalResponse.setDescription("Operation Success.");
                generalResponse.setExiste(response);
            }
        }catch (PersistenceException e) {
            generalResponse.setCode("-1");
            generalResponse.setDescription(e.getMessage());
            LOGGER.error("Ha ocurrido un error al consultar en la base de datos.");
	}finally{
            if(manager != null){
                try {
                    manager.close();
                } catch (PersistenceException e) {
                    LOGGER.error("No se pudo cerrar la base de datos");
                }
            }
        }
        Gson json = new Gson();
        return Response
            .status(200)
            .header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
            .header("Access-Control-Allow-Credentials", "true")
            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
            .header("Access-Control-Max-Age", "1209600")
            .entity(json.toJson(generalResponse))
            .build();
    }
    
    
    @GET
    @Path("/getAllAcusesFull/{periodo}/{ciclo}/{tipo}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllRowAcuses(@PathParam("periodo") String periodo,@PathParam("ciclo") String ciclo,@PathParam("tipo") String tipo) {
        InvoiceManager manager = null;
        List<AcuseRowDTO> response = null;
        GeneralResponse generalResponse = new GeneralResponse();
        try{
            ServiceSessionEJBLocal<InvoiceManager> serviceSession = ServiceSessionEJB.getInstance();
            manager = (InvoiceManager) serviceSession.getSessionDataSource(InvoiceManager.class, "Invoice");
            response = manager.getAllRowAcuses(periodo,ciclo,tipo);
            if(response.isEmpty()){
                generalResponse.setCode("100");
                generalResponse.setDescription("No se encontraron datos.");
                generalResponse.setFullRowAcuses(response);
            }else{
                generalResponse.setCode("0");
                generalResponse.setDescription("Operation Success.");
                generalResponse.setFullRowAcuses(response);
            }
        }catch (PersistenceException e) {
            generalResponse.setCode("-1");
            generalResponse.setDescription(e.getMessage());
            LOGGER.error("Ha ocurrido un error al consultar en la base de datos.");
	}finally{
            if(manager != null){
                try {
                    manager.close();
                } catch (PersistenceException e) {
                    LOGGER.error("No se pudo cerrar la base de datos");
                }
            }
        }
        Gson json = new Gson();
        return Response
            .status(200)
            .header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
            .header("Access-Control-Allow-Credentials", "true")
            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
            .header("Access-Control-Max-Age", "1209600")
            .entity(json.toJson(generalResponse))
            .build();
    }
    
    
    @GET
    @Path("/getAllAcuses/{periodo}/{ciclo}/{tipo}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllAcuses(@PathParam("periodo") String periodo,@PathParam("ciclo") String ciclo,@PathParam("tipo") String tipo) {
        InvoiceManager manager = null;
        List<AcuseListDTO> response = null;
        GeneralResponse generalResponse = new GeneralResponse();
        try{
            ServiceSessionEJBLocal<InvoiceManager> serviceSession = ServiceSessionEJB.getInstance();
            manager = (InvoiceManager) serviceSession.getSessionDataSource(InvoiceManager.class, "Invoice");
            response = manager.getAcuses(periodo,ciclo,tipo);
            if(response.isEmpty()){
                generalResponse.setCode("100");
                generalResponse.setDescription("No se encontraron datos.");
                generalResponse.setResponseAcuses(response);
            }else{
                generalResponse.setCode("0");
                generalResponse.setDescription("Operation Success.");
                generalResponse.setResponseAcuses(response);
            }
        }catch (PersistenceException e) {
            generalResponse.setCode("-1");
            generalResponse.setDescription(e.getMessage());
            LOGGER.error("Ha ocurrido un error al consultar en la base de datos.");
	}finally{
            if(manager != null){
                try {
                    manager.close();
                } catch (PersistenceException e) {
                    LOGGER.error("No se pudo cerrar la base de datos");
                }
            }
        }
        Gson json = new Gson();
        return Response
            .status(200)
            .header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
            .header("Access-Control-Allow-Credentials", "true")
            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
            .header("Access-Control-Max-Age", "1209600")
            .entity(json.toJson(generalResponse))
            .build();
    }
    
    /// LUEGO MOVERLO A OTRA RUTA YA QUE SON LOS ENDPOINT DE ACUSES
    @GET
    @Path("/getStatsMensajerosByPerCycle/{periodo}/{ciclo}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStatsMensajerosByPerCycle(@PathParam("periodo") String periodo,@PathParam("ciclo") String ciclo) {
        InvoiceManager manager = null;
        List<AllstatsMensByPerCicloDTO> response = null;
        GeneralResponse generalResponse = new GeneralResponse();
        try{
            ServiceSessionEJBLocal<InvoiceManager> serviceSession = ServiceSessionEJB.getInstance();
            manager = (InvoiceManager) serviceSession.getSessionDataSource(InvoiceManager.class, "Invoice");
            response = manager.getAllStatsMensajeros(periodo,ciclo);
            if(response.isEmpty()){
                generalResponse.setCode("100");
                generalResponse.setDescription("No se encontraron datos.");
                generalResponse.setResponseStatsMensajeros(response);
            }else{
                generalResponse.setCode("0");
                generalResponse.setDescription("Operation Success.");
                generalResponse.setResponseStatsMensajeros(response);
            }
        }catch (PersistenceException e) {
            generalResponse.setCode("-1");
            generalResponse.setDescription(e.getMessage());
            LOGGER.error("Ha ocurrido un error al consultar en la base de datos.");
	}finally{
            if(manager != null){
                try {
                    manager.close();
                } catch (PersistenceException e) {
                    LOGGER.error("No se pudo cerrar la base de datos");
                }
            }
        }
        Gson json = new Gson();
        return Response
            .status(200)
            .header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
            .header("Access-Control-Allow-Credentials", "true")
            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
            .header("Access-Control-Max-Age", "1209600")
            .entity(json.toJson(generalResponse))
            .build();
    }
    
    
    @GET
    @Path("/getStatsAcusesByPerCycle/{periodo}/{ciclo}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStatsAcusesByPerCycle(@PathParam("periodo") String periodo,@PathParam("ciclo") String ciclo) {
        InvoiceManager manager = null;
        List<AcuseAllStatsDTO> response = null;
        GeneralResponse generalResponse = new GeneralResponse();
        try{
            ServiceSessionEJBLocal<InvoiceManager> serviceSession = ServiceSessionEJB.getInstance();
            manager = (InvoiceManager) serviceSession.getSessionDataSource(InvoiceManager.class, "Invoice");
            response = manager.getAllStatsAcuses(periodo,ciclo);
            if(response.isEmpty()){
                generalResponse.setCode("100");
                generalResponse.setDescription("No se encontraron datos.");
                generalResponse.setResponseStatsAcuses(response);
            }else{
                generalResponse.setCode("0");
                generalResponse.setDescription("Operation Success.");
                generalResponse.setResponseStatsAcuses(response);
            }
        }catch (PersistenceException e) {
            generalResponse.setCode("-1");
            generalResponse.setDescription(e.getMessage());
            LOGGER.error("Ha ocurrido un error al consultar en la base de datos.");
	}finally{
            if(manager != null){
                try {
                    manager.close();
                } catch (PersistenceException e) {
                    LOGGER.error("No se pudo cerrar la base de datos");
                }
            }
        }
        Gson json = new Gson();
        return Response
            .status(200)
            .header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
            .header("Access-Control-Allow-Credentials", "true")
            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
            .header("Access-Control-Max-Age", "1209600")
            .entity(json.toJson(generalResponse))
            .build();
    }
    
    
    @GET
    @Path("/getAllPerCiclos/")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getPerCiclos() {
        InvoiceManager manager = null;
        List<AcuseAllPerCiclosDTO> response = null;
        GeneralResponse generalResponse = new GeneralResponse();
        try{
            ServiceSessionEJBLocal<InvoiceManager> serviceSession = ServiceSessionEJB.getInstance();
            manager = (InvoiceManager) serviceSession.getSessionDataSource(InvoiceManager.class, "Invoice");
            response = manager.getAllPerCiclos();
            if(response.isEmpty()){
                generalResponse.setCode("100");
                generalResponse.setDescription("No se encontraron datos.");
                generalResponse.setResponseAllPerCiclos(response);
            }else{
                generalResponse.setCode("0");
                generalResponse.setDescription("Operation Success.");
                generalResponse.setResponseAllPerCiclos(response);
            }
        }catch (PersistenceException e) {
            generalResponse.setCode("-1");
            generalResponse.setDescription(e.getMessage());
            LOGGER.error("Ha ocurrido un error al consultar en la base de datos.");
	}finally{
            if(manager != null){
                try {
                    manager.close();
                } catch (PersistenceException e) {
                    LOGGER.error("No se pudo cerrar la base de datos");
                }
            }
        }
        Gson json = new Gson();
        return Response
            .status(200)
            .header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
            .header("Access-Control-Allow-Credentials", "true")
            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
            .header("Access-Control-Max-Age", "1209600")
            .entity(json.toJson(generalResponse))
            .build();
    }
    
    @GET
    @Path("/getAllMensajeros/")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getAllMensajeros() {
        InvoiceManager manager = null;
        List<AcuseAllMensajerosDTO> response = null;
        GeneralResponse generalResponse = new GeneralResponse();
        try{
            ServiceSessionEJBLocal<InvoiceManager> serviceSession = ServiceSessionEJB.getInstance();
            manager = (InvoiceManager) serviceSession.getSessionDataSource(InvoiceManager.class, "Invoice");
            response = manager.getAllMensajeros();
            if(response.isEmpty()){
                generalResponse.setCode("100");
                generalResponse.setDescription("No se encontraron datos.");
                generalResponse.setResponseMensajeros(response);
            }else{
                generalResponse.setCode("0");
                generalResponse.setDescription("Operation Success.");
                generalResponse.setResponseMensajeros(response);
            }
        }catch (PersistenceException e) {
            generalResponse.setCode("-1");
            generalResponse.setDescription(e.getMessage());
            LOGGER.error("Ha ocurrido un error al consultar en la base de datos.");
	}finally{
            if(manager != null){
                try {
                    manager.close();
                } catch (PersistenceException e) {
                    LOGGER.error("No se pudo cerrar la base de datos");
                }
            }
        }
        Gson json = new Gson();
        return Response
            .status(200)
            .header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
            .header("Access-Control-Allow-Credentials", "true")
            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
            .header("Access-Control-Max-Age", "1209600")
            .entity(json.toJson(generalResponse))
            .build();
    }
    
    @GET
    @Path("/getAllMotivos/")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getAllMotivos() {
        InvoiceManager manager = null;
        List<AcuseAllMotivosDTO> response = null;
        GeneralResponse generalResponse = new GeneralResponse();
        try{
            ServiceSessionEJBLocal<InvoiceManager> serviceSession = ServiceSessionEJB.getInstance();
            manager = (InvoiceManager) serviceSession.getSessionDataSource(InvoiceManager.class, "Invoice");
            response = manager.getAllMotivos();
            if(response.isEmpty()){
                generalResponse.setCode("100");
                generalResponse.setDescription("No se encontraron datos.");
                generalResponse.setResponseMotivos(response);
            }else{
                generalResponse.setCode("0");
                generalResponse.setDescription("Operation Success.");
                generalResponse.setResponseMotivos(response);
            }
        }catch (PersistenceException e) {
            generalResponse.setCode("-1");
            generalResponse.setDescription(e.getMessage());
            LOGGER.error("Ha ocurrido un error al consultar en la base de datos.");
	}finally{
            if(manager != null){
                try {
                    manager.close();
                } catch (PersistenceException e) {
                    LOGGER.error("No se pudo cerrar la base de datos");
                }
            }
        }
        Gson json = new Gson();
        return Response
            .status(200)
            .header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
            .header("Access-Control-Allow-Credentials", "true")
            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
            .header("Access-Control-Max-Age", "1209600")
            .entity(json.toJson(generalResponse))
            .build();
    }
    
    @GET
    @Path("/getInvoicePDF/{subscriber}/{cycle}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getByUUID(@PathParam("subscriber") String subscriber, @PathParam("cycle") String cycle) {
        InvoiceManager manager = null;
        String invoiceBase64 = null;
        try{
            LOGGER.info("Inicio del proceso: " + Calendar.getInstance().getTime());
            validateParameter("subscriber", subscriber, AdapterValidationType.NUMERIC, true);
            validateParameter("current", cycle, AdapterValidationType.NUMERIC, true);
            ServiceSessionEJBLocal<InvoiceManager> serviceSession = ServiceSessionEJB.getInstance();
            manager = (InvoiceManager) serviceSession.getSessionDataSource(InvoiceManager.class, "Invoice");
            invoiceBase64 = manager.getInvoice(subscriber, cycle);
            if(invoiceBase64 == null){
                invoiceBase64 = "No se encontro Factura";
            }
        }catch (PersistenceException e) {
            invoiceBase64 = e.getMessage();
            LOGGER.error("Ha ocurrido un error al consultar en la base de datos.");
	}catch (Exception e) {
            invoiceBase64 = e.getMessage();
            LOGGER.error("Los parametros no son validos.");
	}finally{
            if(manager != null){
                try {
                    manager.close();
                } catch (PersistenceException e) {
                    LOGGER.error("No se pudo cerrar la base de datos");
                }
            }
        }
        LOGGER.info("Fin del proceso: " + Calendar.getInstance().getTime());
        return Response
            .status(200)
            .header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
            .header("Access-Control-Allow-Credentials", "true")
            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
            .header("Access-Control-Max-Age", "1209600")
            .entity(invoiceBase64)
            .build();
    }
    
    @GET
    @Path("/getInvoiceB64PDF/{uuid}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getpdfByUUID(@PathParam("uuid") String uuid) {
        InvoiceManager manager = null;
        String invoiceBase64 = null;
        try{
            LOGGER.info("Inicio del proceso: " + Calendar.getInstance().getTime());
            //validateParameter("subscriber", subscriber, AdapterValidationType.NUMERIC, true);
            //validateParameter("current", cycle, AdapterValidationType.NUMERIC, true);
            ServiceSessionEJBLocal<InvoiceManager> serviceSession = ServiceSessionEJB.getInstance();
            manager = (InvoiceManager) serviceSession.getSessionDataSource(InvoiceManager.class, "Invoice");
            invoiceBase64 = manager.getInvoiceB64(uuid);
            if(invoiceBase64 == null){
                invoiceBase64 = "No se encontro Factura";
            }
        }catch (PersistenceException e) {
            invoiceBase64 = e.getMessage();
            LOGGER.error("Ha ocurrido un error al consultar en la base de datos.");
	}catch (Exception e) {
            invoiceBase64 = e.getMessage();
            LOGGER.error("Los parametros no son validos.");
	}finally{
            if(manager != null){
                try {
                    manager.close();
                } catch (PersistenceException e) {
                    LOGGER.error("No se pudo cerrar la base de datos");
                }
            }
        }
        LOGGER.info("Fin del proceso: " + Calendar.getInstance().getTime());
        return Response
            .status(200)
            .header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
            .header("Access-Control-Allow-Credentials", "true")
            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
            .header("Access-Control-Max-Age", "1209600")
            .entity(invoiceBase64)
            .build();
    }
    
    @GET
    @Path("/getStatsByCycle/{cycle}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStatsByCycle(@PathParam("cycle") String cycle) {
        InvoiceManager manager = null;
        List<InvoiceStats> response = null;
        GeneralResponse generalResponse = new GeneralResponse();
        try{
            ServiceSessionEJBLocal<InvoiceManager> serviceSession = ServiceSessionEJB.getInstance();
            manager = (InvoiceManager) serviceSession.getSessionDataSource(InvoiceManager.class, "Invoice");
            response = manager.getStatsCycle(cycle);
            if(response.isEmpty()){
                generalResponse.setCode("100");
                generalResponse.setDescription("No se encontraron datos.");
                generalResponse.setResponseStats(response);
            }else{
                generalResponse.setCode("0");
                generalResponse.setDescription("Operation Success.");
                generalResponse.setResponseStats(response);
            }
        }catch (PersistenceException e) {
            generalResponse.setCode("-1");
            generalResponse.setDescription(e.getMessage());
            LOGGER.error("Ha ocurrido un error al consultar en la base de datos.");
	}finally{
            if(manager != null){
                try {
                    manager.close();
                } catch (PersistenceException e) {
                    LOGGER.error("No se pudo cerrar la base de datos");
                }
            }
        }
        Gson json = new Gson();
        return Response
            .status(200)
            .header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
            .header("Access-Control-Allow-Credentials", "true")
            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
            .header("Access-Control-Max-Age", "1209600")
            .entity(json.toJson(generalResponse))
            .build();
    }
    
    @GET
    @Path("/getAllStats/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllStats() {
       InvoiceManager manager = null;
        List<AllStatsDTO> response = null;
        GeneralResponse generalResponse = new GeneralResponse();
        try{
            ServiceSessionEJBLocal<InvoiceManager> serviceSession = ServiceSessionEJB.getInstance();
            manager = (InvoiceManager) serviceSession.getSessionDataSource(InvoiceManager.class, "Invoice");
            response = manager.getAllStats();
            if(response.isEmpty()){
                generalResponse.setCode("100");
                generalResponse.setDescription("GENERAR LA TABLA CON LOS DATOS");
                generalResponse.setResponseAllStats(response);
            }else{
                generalResponse.setCode("0");
                generalResponse.setDescription("Operation Success.");
                generalResponse.setResponseAllStats(response);
            }
        }catch (PersistenceException e) {
            generalResponse.setCode("-1");
            generalResponse.setDescription(e.getMessage());
            LOGGER.error("Ha ocurrido un error al consultar en la base de datos.");
	}finally{
            if(manager != null){
                try {
                    manager.close();
                } catch (PersistenceException e) {
                    LOGGER.error("No se pudo cerrar la base de datos");
                }
            }
        }
        Gson json = new Gson();
        return Response
            .status(200)
            .header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
            .header("Access-Control-Allow-Credentials", "true")
            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
            .header("Access-Control-Max-Age", "1209600")
            .entity(json.toJson(generalResponse))
            .build();
    }
    
    @GET
    @Path("/queryByCustId/{custId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustId(@PathParam("custId") String custId) {
        InvoiceManager manager = null;
        List<InvoiceList> response = null;
        GeneralResponse generalResponse = new GeneralResponse();
        try{
            ServiceSessionEJBLocal<InvoiceManager> serviceSession = ServiceSessionEJB.getInstance();
            manager = (InvoiceManager) serviceSession.getSessionDataSource(InvoiceManager.class, "Invoice");
            response = manager.getInvoiceCustId(custId);
            if(response.isEmpty()){
                generalResponse.setCode("100");
                generalResponse.setDescription("No se encontraron datos.");
                generalResponse.setResponse(response);
            }else{
                generalResponse.setCode("0");
                generalResponse.setDescription("Operation Success.");
                generalResponse.setResponse(response);
            }
        }catch (PersistenceException e) {
            generalResponse.setCode("-1");
            generalResponse.setDescription(e.getMessage());
            LOGGER.error("Ha ocurrido un error al consultar en la base de datos.");
	}finally{
            if(manager != null){
                try {
                    manager.close();
                } catch (PersistenceException e) {
                    LOGGER.error("No se pudo cerrar la base de datos");
                }
            }
        }
        Gson json = new Gson();
        return Response
            .status(200)
            .header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
            .header("Access-Control-Allow-Credentials", "true")
            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
            .header("Access-Control-Max-Age", "1209600")
            .entity(json.toJson(generalResponse))
            .build();
    }
    
    @GET
    @Path("/queryByCustCodeAndBillCycle")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustCodeAndBillCycle(@QueryParam("custCode") String custCode, @QueryParam("billCycle") String billCycle) {
        InvoiceManager manager = null;
        List<InvoiceList> response = null;
        try{
            ServiceSessionEJBLocal<InvoiceManager> serviceSession = ServiceSessionEJB.getInstance();
            manager = (InvoiceManager) serviceSession.getSessionDataSource(InvoiceManager.class, "Invoice");
            response = manager.getInvoiceCustCodeAndBillCycle(custCode, billCycle);
        }catch (PersistenceException e) {
            LOGGER.error("Ha ocurrido un error al consultar en la base de datos.");
	}finally{
            if(manager != null){
                try {
                    manager.close();
                } catch (PersistenceException e) {
                    LOGGER.error("No se pudo cerrar la base de datos");
                }
            }
        }
        Gson json = new Gson();
        return Response
            .status(200)
            .header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
            .header("Access-Control-Allow-Credentials", "true")
            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
            .header("Access-Control-Max-Age", "1209600")
            .entity(json.toJson(response))
            .build();
    }
    
    @GET
    @Path("/queryByCustIdAndBillCycle")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustIdAndBillCycle(@QueryParam("custId") String custId, @QueryParam("billCycle") String billCycle) {
        InvoiceManager manager = null;
        List<InvoiceList> response = null;
        try{
            ServiceSessionEJBLocal<InvoiceManager> serviceSession = ServiceSessionEJB.getInstance();
            manager = (InvoiceManager) serviceSession.getSessionDataSource(InvoiceManager.class, "Invoice");
            response = manager.getInvoiceCustIdAndBillCycle(custId, billCycle);
        }catch (PersistenceException e) {
            LOGGER.error("Ha ocurrido un error al consultar en la base de datos.");
	}finally{
            if(manager != null){
                try {
                    manager.close();
                } catch (PersistenceException e) {
                    LOGGER.error("No se pudo cerrar la base de datos");
                }
            }
        }
        Gson json = new Gson();
        return Response
            .status(200)
            .header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
            .header("Access-Control-Allow-Credentials", "true")
            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
            .header("Access-Control-Max-Age", "1209600")
            .entity(json.toJson(response))
            .build();
    }
    
    @GET
    @Path("/queryBySubscriber/{subscriber}/{current}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBySubscriber(@PathParam("subscriber") String subscriber, @PathParam("current") String current) {
        InvoiceManager manager = null;
        List<InvoiceList> response = null;
        GeneralResponse generalResponse = new GeneralResponse();
        try{
            validateParameter("subscriber", subscriber, AdapterValidationType.NUMERIC, true);
            validateParameter("current", current, AdapterValidationType.NUMERIC, true);
            if(Long.valueOf(current) > 1 || Long.valueOf(current) < 0){
                generalResponse.setCode("-1");
                generalResponse.setDescription("current no valido.");
            }else{
                ServiceSessionEJBLocal<InvoiceManager> serviceSession = ServiceSessionEJB.getInstance();
                manager = (InvoiceManager) serviceSession.getSessionDataSource(InvoiceManager.class, "Invoice");
                response = manager.getInvoiceSubscriber(String.valueOf(subscriber), current);
                if(response.isEmpty()){
                    generalResponse.setCode("-1");
                    generalResponse.setDescription("No se encontraron datos.");
                    generalResponse.setResponse(response);
                }else{
                    generalResponse.setCode("0");
                    generalResponse.setDescription("Operation Success.");
                    generalResponse.setResponse(response);
                }
            }
        }catch (PersistenceException e) {
            generalResponse.setCode("-1");
            generalResponse.setDescription(e.getMessage());
            LOGGER.error("Ha ocurrido un error al consultar en la base de datos.");
	}catch (Exception e) {
            generalResponse.setCode("-1");
            generalResponse.setDescription(e.getMessage());
            LOGGER.error("Ha ocurrido un error al consultar en la base de datos.");
	}finally{
            if(manager != null){
                try {
                    manager.close();
                } catch (PersistenceException e) {
                    LOGGER.error("No se pudo cerrar la base de datos");
                }
            }
        }
        Gson json = new Gson();
        return Response
            .status(200)
            .header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
            .header("Access-Control-Allow-Credentials", "true")
            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
            .header("Access-Control-Max-Age", "1209600")
            .entity(json.toJson(generalResponse))
            .build();
    }
    
    @GET
    @Path("/getInvoiceStatus/{subscriber}/{current}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getInvoiceStatus(@PathParam("subscriber") String subscriber, @PathParam("current") String current) {
        InvoiceManager manager = null;
        List<InvoiceList> response = null;
        GeneralResponse generalResponse = new GeneralResponse();
        try{
            validateParameter("subscriber", subscriber, AdapterValidationType.NUMERIC, true);
            validateParameter("current", current, AdapterValidationType.NUMERIC, true);
            if(Long.valueOf(current) > 1 || Long.valueOf(current) < 0){
                generalResponse.setCode("-1");
                generalResponse.setDescription("current no valido.");
            }else{
                ServiceSessionEJBLocal<InvoiceManager> serviceSession = ServiceSessionEJB.getInstance();
                manager = (InvoiceManager) serviceSession.getSessionDataSource(InvoiceManager.class, "Invoice");
                response = manager.getInvoiceSubscriber(String.valueOf(subscriber), current);
                if(response.isEmpty()){
                    generalResponse.setCode("-1");
                    generalResponse.setDescription("No se encontraron datos.");
                    generalResponse.setResponse(response);
                }else{
                    generalResponse.setCode("0");
                    generalResponse.setDescription("Operation Success.");
                    List<InvoiceStatusModel> detailInvoice = new ArrayList<>();
                    for (InvoiceList detail : response) {
                        InvoiceStatusModel model = new InvoiceStatusModel();
                        model.setId(detail.getUuid());
                        model.setPrintedInvoiceDetails(detail.getInvoicebase64()!=null?"SI":"NO");
                        model.setPrintedInvoiceStatus(detail.getInvoicebase64()!=null?"SI":"NO");
                        model.setElectronicInvoiceStatus(detail.getInvoicebase64()!=null?"SI":"NO");
                        model.setElectronicInvoiceDetails(detail.getInvoicebase64()!=null?"SI":"NO");
                        detailInvoice.add(model);
                    }
                    //generalResponse.setResponse(response);
                    generalResponse.setResponseStatus(detailInvoice);
                }
            }
        }catch (PersistenceException e) {
            generalResponse.setCode("-1");
            generalResponse.setDescription(e.getMessage());
            LOGGER.error("Ha ocurrido un error al consultar en la base de datos.");
	}catch (Exception e) {
            generalResponse.setCode("-1");
            generalResponse.setDescription(e.getMessage());
            LOGGER.error("Ha ocurrido un error al consultar en la base de datos.");
	}finally{
            if(manager != null){
                try {
                    manager.close();
                } catch (PersistenceException e) {
                    LOGGER.error("No se pudo cerrar la base de datos");
                }
            }
        }
        Gson json = new Gson();
        return Response
            .status(200)
            .header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
            .header("Access-Control-Allow-Credentials", "true")
            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
            .header("Access-Control-Max-Age", "1209600")
            .entity(json.toJson(generalResponse))
            .build();
    }
    
    private void validateParameter(final String paramName, final String paramValue,
			final AdapterValidationType validationType, final boolean required) throws Exception {
	validateParameterAll(paramName, paramValue, validationType.getValidationExpression(), required);
    }
    
    /**
	 * Method responsible for validating a specific parameter of the
	 * {@link TaskRequestType} instance.
	 *
	 * @param paramName
	 *            the parameter name
	 * @param paramValue
	 *            the parameter value
	 * @param expression
	 *            the regular expression to which this string is to be matched
	 * @param required
	 *            the required parameter indicates if MUST be validated
     * @throws java.lang.Exception
	 * @see java.util.regex.Pattern.matches(regex, str)
	 */
	protected void validateParameterAll(final String paramName, final String paramValue, final String expression,
			final boolean required) throws Exception {

		if (required && paramValue == null) {
			throw new Exception("No se encontro el parameto: " + paramName);
		}

		if (required && paramValue.isEmpty()) {
			throw new Exception("Invalid Param: " + paramName);
		}

		if (paramValue != null && !paramValue.isEmpty() && expression != null && !paramValue.matches(expression)) {
			throw new Exception("Invalid Param: " + paramName);
		}

	}

}
