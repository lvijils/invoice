package hn.com.tigo.invoice.dto;

public class AcuseBycicloPerClienteDTO extends DTO {

    private long id;
    private long id_periodo;

    public long getExiste() {
        return id_periodo;
    }

    public void setExiste(long id_periodo) {
        this.id_periodo = id_periodo;
    }

    public long getID() {
        return id;
    }

    public void setID(long id) {
        this.id = id;
    }
}
