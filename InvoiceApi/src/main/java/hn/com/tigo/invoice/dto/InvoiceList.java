package hn.com.tigo.invoice.dto;

import java.util.Date;

public class InvoiceList extends DTO {

    private String uuid; // Tipo DB: NVARCHAR2
    private String xmlname; // Tipo DB: NVARCHAR2
    private String billingcycleid; // Tipo DB: NVARCHAR2
    private String billingtype; // Tipo DB: NVARCHAR2
    private String company; // Tipo DB: NVARCHAR2
    private String custcode; // Tipo DB: NVARCHAR2
    private String custid; // Tipo DB: NVARCHAR2
    private String custtype; // Tipo DB: NVARCHAR2
    private Date datetime; // Tipo DB: DATE
    private String emailaddr; // Tipo DB: NVARCHAR2
    private String exchangerate; // Tipo DB: NVARCHAR2
    private long idprocess; // Tipo DB: NUMBER
    private String invoicebase64; // Tipo DB: CLOB
    private String jsonstr; // Tipo DB: CLOB
    private String smsaddr; // Tipo DB: NVARCHAR2
    private String xml; // Tipo DB: CLOB
    private String invoiceid; // Tipo DB: VARCHAR2
    private String billmedium; // Tipo DB: VARCHAR2
    private String charge; // Tipo DB: VARCHAR2
    private String responsevoucher; // Tipo DB: CLOB
    private Date created; // Tipo DB: DATE
    private String primaryidentity; // Tipo DB: VARCHAR2
    private String prevbalance; // Tipo DB: VARCHAR2
    private String billCycleBegin;
    private String billCyleEnd;
    private String name;
    private String address;
    private String subTotal;

    public InvoiceList() {
    }

    public final String getUuid() {
        return uuid;
    }

    public final void setUuid(final String uuid) {
        this.uuid = uuid;
    }

    public final String getXmlname() {
        return xmlname;
    }

    public final void setXmlname(final String xmlname) {
        this.xmlname = xmlname;
    }

    public final String getBillingcycleid() {
        return billingcycleid;
    }

    public final void setBillingcycleid(final String billingcycleid) {
        this.billingcycleid = billingcycleid;
    }

    public final String getBillingtype() {
        return billingtype;
    }

    public final void setBillingtype(final String billingtype) {
        this.billingtype = billingtype;
    }

    public final String getCompany() {
        return company;
    }

    public final void setCompany(final String company) {
        this.company = company;
    }

    public final String getCustcode() {
        return custcode;
    }

    public final void setCustcode(final String custcode) {
        this.custcode = custcode;
    }

    public final String getCustid() {
        return custid;
    }

    public final void setCustid(final String custid) {
        this.custid = custid;
    }

    public final String getCusttype() {
        return custtype;
    }

    public final void setCusttype(final String custtype) {
        this.custtype = custtype;
    }

    public final Date getDatetime() {
        return datetime;
    }

    public final void setDatetime(final Date datetime) {
        this.datetime = datetime;
    }

    public final String getEmailaddr() {
        return emailaddr;
    }

    public final void setEmailaddr(final String emailaddr) {
        this.emailaddr = emailaddr;
    }

    public final String getExchangerate() {
        return exchangerate;
    }

    public final void setExchangerate(final String exchangerate) {
        this.exchangerate = exchangerate;
    }

    public final long getIdprocess() {
        return idprocess;
    }

    public final void setIdprocess(final long idprocess) {
        this.idprocess = idprocess;
    }

    public final String getInvoicebase64() {
        return invoicebase64;
    }

    public final void setInvoicebase64(final String invoicebase64) {
        this.invoicebase64 = invoicebase64;
    }

    public final String getJsonstr() {
        return jsonstr;
    }

    public final void setJsonstr(final String jsonstr) {
        this.jsonstr = jsonstr;
    }

    public final String getSmsaddr() {
        return smsaddr;
    }

    public final void setSmsaddr(final String smsaddr) {
        this.smsaddr = smsaddr;
    }

    public final String getXml() {
        return xml;
    }

    public final void setXml(final String xml) {
        this.xml = xml;
    }

    public final String getInvoiceid() {
        return invoiceid;
    }

    public final void setInvoiceid(final String invoiceid) {
        this.invoiceid = invoiceid;
    }

    public final String getBillmedium() {
        return billmedium;
    }

    public final void setBillmedium(final String billmedium) {
        this.billmedium = billmedium;
    }

    public final String getCharge() {
        return charge;
    }

    public final void setCharge(final String charge) {
        this.charge = charge;
    }

    public final String getResponsevoucher() {
        return responsevoucher;
    }

    public final void setResponsevoucher(final String responsevoucher) {
        this.responsevoucher = responsevoucher;
    }

    public final Date getCreated() {
        return created;
    }

    public final void setCreated(final Date created) {
        this.created = created;
    }

    public final String getPrimaryidentity() {
        return primaryidentity;
    }

    public final void setPrimaryidentity(final String primaryidentity) {
        this.primaryidentity = primaryidentity;
    }

    public final String getPrevbalance() {
        return prevbalance;
    }

    public final void setPrevbalance(final String prevbalance) {
        this.prevbalance = prevbalance;
    }

    public String getBillCycleBegin() {
        return billCycleBegin;
    }

    public void setBillCycleBegin(String billCycleBegin) {
        this.billCycleBegin = billCycleBegin;
    }

    public String getBillCyleEnd() {
        return billCyleEnd;
    }

    public void setBillCyleEnd(String billCyleEnd) {
        this.billCyleEnd = billCyleEnd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

}
