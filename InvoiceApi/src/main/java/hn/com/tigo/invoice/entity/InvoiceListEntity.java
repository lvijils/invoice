package hn.com.tigo.invoice.entity;

import hn.com.tigo.invoice.dto.InvoiceList;
import hn.com.tigo.josm.persistence.core.EntityBase;
import hn.com.tigo.josm.persistence.core.SessionBase;
import hn.com.tigo.josm.persistence.exception.PersistenceError;
import hn.com.tigo.josm.persistence.exception.PersistenceException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class InvoiceListEntity extends EntityBase<InvoiceList> {

    private final String _coreSelect;
    private final String _coreSelectByUUID;

    public InvoiceListEntity(final SessionBase sessionBase) throws PersistenceException {
        super(sessionBase);
        _coreSelect = "SELECT invoicebase64 FROM INVOICE_LIST WHERE PRIMARYIDENTITY = ? and BILLINGCYCLEID = ?";
        _coreSelectByUUID = "SELECT invoicebase64 FROM INVOICE_LIST WHERE UUID = ?";
    }

    @Override
    protected final ArrayList<InvoiceList> fillList(ResultSet result) throws PersistenceException {
        InvoiceList dto;
        ArrayList<InvoiceList> lista = new ArrayList<InvoiceList>();
        try {
            while (result.next()) {
                dto = new InvoiceList();
                dto.setInvoicebase64(result.getString("invoicebase64"));
                lista.add(dto);
            }
            return lista;
        } catch (SQLException e) {
            throw new PersistenceException(PersistenceError.SQL, e);
        }
    }

    public final List<InvoiceList> getInvoice(final String subscriber, final String cycle) throws PersistenceException {
        return executeSelectStatement(_coreSelect, subscriber, cycle);
    }

    public final List<InvoiceList> getInvoiceB64(final String uuid) throws PersistenceException {
        return executeSelectStatement(_coreSelectByUUID, uuid);
    }

}
