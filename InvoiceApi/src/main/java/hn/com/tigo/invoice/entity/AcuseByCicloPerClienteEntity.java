package hn.com.tigo.invoice.entity;

import hn.com.tigo.invoice.dto.AcuseBycicloPerClienteDTO;
import hn.com.tigo.josm.persistence.core.EntityBase;
import hn.com.tigo.josm.persistence.core.SessionBase;
import hn.com.tigo.josm.persistence.exception.PersistenceError;
import hn.com.tigo.josm.persistence.exception.PersistenceException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AcuseByCicloPerClienteEntity extends EntityBase<AcuseBycicloPerClienteDTO> {

    private final String _coreSelectExiste;

    public AcuseByCicloPerClienteEntity(final SessionBase sessionBase) throws PersistenceException {
        super(sessionBase);
        _coreSelectExiste = "SELECT id,id_periodo FROM REGISTRO_ACUSES WHERE id_periodo=? AND id_ciclo=? AND cliente=? AND ID!=?";
    }

    @Override
    protected List<AcuseBycicloPerClienteDTO> fillList(ResultSet rs) throws PersistenceException {
        AcuseBycicloPerClienteDTO dto;
        ArrayList<AcuseBycicloPerClienteDTO> lista = new ArrayList<AcuseBycicloPerClienteDTO>();
        dto = new AcuseBycicloPerClienteDTO();
        try {
            while (rs.next()) {
                dto = new AcuseBycicloPerClienteDTO();
                dto.setID(rs.getLong("id"));
                dto.setExiste(rs.getLong("id_periodo"));
                lista.add(dto);
            }
            return lista;
        } catch (SQLException e) {
            throw new PersistenceException(PersistenceError.SQL, e);
        }

    }

    public final List<AcuseBycicloPerClienteDTO> getExisteCliente(final String periodo, final String cycle, final String cliente, final String id) throws PersistenceException {
        return executeSelectStatement(_coreSelectExiste, periodo, cycle, cliente, id);//fecha
    }
}
