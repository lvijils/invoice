package hn.com.tigo.invoice.dto;

public class AcuseRowDTO extends DTO {

    private long id;
    private String id_periodo;
    private String nombreCiclo;
    private String fecha;
    private String cliente;
    private String nombreMensajero;
    private long entregado;
    private String NombreMotivo;
    private String observacion;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getId_periodo() {
        return id_periodo;
    }

    public void setId_periodo(String id_periodo) {
        this.id_periodo = id_periodo;
    }

    public String getNombreCiclo() {
        return nombreCiclo;
    }

    public void setNombreCiclo(String nombreCiclo) {
        this.nombreCiclo = nombreCiclo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getNombreMensajero() {
        return nombreMensajero;
    }

    public void setNombreMensajero(String nombreMensajero) {
        this.nombreMensajero = nombreMensajero;
    }

    public long getEntregado() {
        return entregado;
    }

    public void setEntregado(long entregado) {
        this.entregado = entregado;
    }

    public String getNombreMotivo() {
        return NombreMotivo;
    }

    public void setNombreMotivo(String NombreMotivo) {
        this.NombreMotivo = NombreMotivo;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
}
