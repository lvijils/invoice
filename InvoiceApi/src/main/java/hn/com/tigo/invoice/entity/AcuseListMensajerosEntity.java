package hn.com.tigo.invoice.entity;

import hn.com.tigo.invoice.dto.AcuseAllMensajerosDTO;
import hn.com.tigo.josm.persistence.core.EntityBase;
import hn.com.tigo.josm.persistence.core.SessionBase;
import hn.com.tigo.josm.persistence.exception.PersistenceError;
import hn.com.tigo.josm.persistence.exception.PersistenceException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AcuseListMensajerosEntity extends EntityBase<AcuseAllMensajerosDTO> {

    private final String _coreSelectMensajeros;

    public AcuseListMensajerosEntity(final SessionBase sessionBase) throws PersistenceException {
        super(sessionBase);
        _coreSelectMensajeros = "SELECT id_mensajero AS id,nombre_mensajero FROM MENSAJEROS_ACUSES";
    }

    @Override
    protected List<AcuseAllMensajerosDTO> fillList(ResultSet rs) throws PersistenceException {
        AcuseAllMensajerosDTO dto;
        ArrayList<AcuseAllMensajerosDTO> lista = new ArrayList<AcuseAllMensajerosDTO>();
        dto = new AcuseAllMensajerosDTO();
        try {
            while (rs.next()) {
                dto = new AcuseAllMensajerosDTO();
                dto.setId(rs.getInt("id"));
                dto.setNombre_mensajero(rs.getString("nombre_mensajero"));
                lista.add(dto);
            }
            return lista;
        } catch (SQLException e) {
            throw new PersistenceException(PersistenceError.SQL, e);
        }

    }

    public final List<AcuseAllMensajerosDTO> getAllMensajeros() throws PersistenceException {
        return executeSelectStatement(_coreSelectMensajeros);
    }
}
