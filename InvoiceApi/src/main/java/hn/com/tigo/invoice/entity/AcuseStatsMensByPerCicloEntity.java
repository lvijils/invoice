package hn.com.tigo.invoice.entity;

import hn.com.tigo.invoice.dto.AllstatsMensByPerCicloDTO;
import hn.com.tigo.josm.persistence.core.EntityBase;
import hn.com.tigo.josm.persistence.core.SessionBase;
import hn.com.tigo.josm.persistence.exception.PersistenceError;
import hn.com.tigo.josm.persistence.exception.PersistenceException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AcuseStatsMensByPerCicloEntity extends EntityBase<AllstatsMensByPerCicloDTO> {

    private final String _coreSelectStatsMensajeros;

    public AcuseStatsMensByPerCicloEntity(final SessionBase sessionBase) throws PersistenceException {
        super(sessionBase);
        _coreSelectStatsMensajeros = "SELECT NOMBRE_MENSAJERO,ENTREGADOS,NO_ENTREGADOS FROM V_STATS_MENSAJEROS WHERE ID_PERIODO=? AND ID_CICLO=?";
    }

    @Override
    protected List<AllstatsMensByPerCicloDTO> fillList(ResultSet rs) throws PersistenceException {
        AllstatsMensByPerCicloDTO dto;
        ArrayList<AllstatsMensByPerCicloDTO> lista = new ArrayList<AllstatsMensByPerCicloDTO>();
        dto = new AllstatsMensByPerCicloDTO();
        try {
            while (rs.next()) {
                dto = new AllstatsMensByPerCicloDTO();
                dto.setNombre_mensajero(rs.getString("nombre_mensajero"));
                dto.setEntregados(rs.getLong("entregados"));
                dto.setNo_entregados(rs.getLong("no_entregados"));
                lista.add(dto);
            }
            return lista;
        } catch (SQLException e) {
            throw new PersistenceException(PersistenceError.SQL, e);
        }

    }

    public final List<AllstatsMensByPerCicloDTO> getAllStatsMensajeros(final String periodo, final String cycle) throws PersistenceException {
        return executeSelectStatement(_coreSelectStatsMensajeros, periodo, cycle);
    }
}
