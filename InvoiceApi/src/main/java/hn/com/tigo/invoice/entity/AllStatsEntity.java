package hn.com.tigo.invoice.entity;

import hn.com.tigo.invoice.dto.AllStatsDTO;
import hn.com.tigo.josm.persistence.core.EntityBase;
import hn.com.tigo.josm.persistence.core.SessionBase;
import hn.com.tigo.josm.persistence.exception.PersistenceError;
import hn.com.tigo.josm.persistence.exception.PersistenceException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AllStatsEntity extends EntityBase<AllStatsDTO> {

    private final String _coreSelectAllStats;

    public AllStatsEntity(final SessionBase sessionBase) throws PersistenceException {
        super(sessionBase);
        _coreSelectAllStats = "SELECT '['||LISTAGG('['||B.CYCLE||',\"'||A.status||'\",'||count(A.status)||']',',')WITHIN GROUP (ORDER BY B.CYCLE)||']' AS res FROM invoice_config.listener_dist_file A INNER JOIN invoice_config.LISTENER_PROCESS_FILE B ON A.ID_PROCESS_FILE=B.ID group by A.status, B.CYCLE";
    }

    @Override
    protected List<AllStatsDTO> fillList(ResultSet rs) throws PersistenceException {
        AllStatsDTO dto;
        ArrayList<AllStatsDTO> lista = new ArrayList<AllStatsDTO>();
        dto = new AllStatsDTO();
        try {
            while (rs.next()) {
                dto = new AllStatsDTO();
                dto.setCyclo(rs.getString("res"));
                lista.add(dto);
            }
            return lista;
        } catch (SQLException e) {
            throw new PersistenceException(PersistenceError.SQL, e);
        }

    }

    public final List<AllStatsDTO> getAllStats() throws PersistenceException {
        return executeSelectStatement(_coreSelectAllStats);
    }
}
