package hn.com.tigo.invoice.entity;

import hn.com.tigo.invoice.dto.AcuseRowDTO;
import hn.com.tigo.josm.persistence.core.EntityBase;
import hn.com.tigo.josm.persistence.core.SessionBase;
import hn.com.tigo.josm.persistence.exception.PersistenceError;
import hn.com.tigo.josm.persistence.exception.PersistenceException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RowAcusesListEntity extends EntityBase<AcuseRowDTO> {

    private final String _coreSelectAllAcuses;    
    private final String _coreSelectRowAcuse;


    public RowAcusesListEntity(final SessionBase sessionBase) throws PersistenceException {
        super(sessionBase);
        _coreSelectAllAcuses = "SELECT ID,REGISTRO_ACUSES.ID_PERIODO,NOMBRE_CICLO,FECHA,CLIENTE,NOMBRE_MENSAJERO,ENTREGADO,NOMBRE_MOTIVO,OBSERVACION FROM REGISTRO_ACUSES JOIN CICLOS_ACUSES ON(REGISTRO_ACUSES.ID_CICLO=CICLOS_ACUSES.ID_CICLO) JOIN MENSAJEROS_ACUSES ON(REGISTRO_ACUSES.ID_MENSAJERO=MENSAJEROS_ACUSES.ID_MENSAJERO) JOIN MOTIVOS_ACUSE ON(REGISTRO_ACUSES.ID_MOTIVO=MOTIVOS_ACUSE.ID_MOTIVO) WHERE REGISTRO_ACUSES.ID_PERIODO=? AND REGISTRO_ACUSES.ID_CICLO=?";
        _coreSelectRowAcuse = "SELECT ID,REGISTRO_ACUSES.ID_PERIODO,NOMBRE_CICLO,FECHA,CLIENTE,NOMBRE_MENSAJERO,ENTREGADO,NOMBRE_MOTIVO,OBSERVACION FROM REGISTRO_ACUSES JOIN CICLOS_ACUSES ON(REGISTRO_ACUSES.ID_CICLO=CICLOS_ACUSES.ID_CICLO) JOIN MENSAJEROS_ACUSES ON(REGISTRO_ACUSES.ID_MENSAJERO=MENSAJEROS_ACUSES.ID_MENSAJERO) JOIN MOTIVOS_ACUSE ON(REGISTRO_ACUSES.ID_MOTIVO=MOTIVOS_ACUSE.ID_MOTIVO) WHERE REGISTRO_ACUSES.ID_PERIODO=? AND REGISTRO_ACUSES.ID_CICLO=? AND REGISTRO_ACUSES.ID=?";
    }

    @Override
    protected List<AcuseRowDTO> fillList(ResultSet rs) throws PersistenceException {
        AcuseRowDTO dto;
        ArrayList<AcuseRowDTO> lista = new ArrayList<AcuseRowDTO>();
        dto = new AcuseRowDTO();
        try {
            while (rs.next()) {
                dto = new AcuseRowDTO();
                dto.setId(rs.getLong("ID"));
                dto.setId_periodo(rs.getString("ID_PERIODO"));
                dto.setNombreCiclo(rs.getString("NOMBRE_CICLO"));
                dto.setFecha(rs.getString("FECHA"));
                dto.setCliente(rs.getString("CLIENTE"));
                dto.setNombreMensajero(rs.getString("NOMBRE_MENSAJERO"));
                dto.setEntregado(rs.getLong("ENTREGADO"));
                dto.setNombreMotivo(rs.getString("NOMBRE_MOTIVO"));
                dto.setObservacion(rs.getString("OBSERVACION"));
                lista.add(dto);
            }
            return lista;
        } catch (SQLException e) {
            throw new PersistenceException(PersistenceError.SQL, e);
        }

    }

    public final List<AcuseRowDTO> getAllAcuses(final String periodo, final String cycle) throws PersistenceException {
        return executeSelectStatement(_coreSelectAllAcuses, periodo, cycle);
    }
    public final List<AcuseRowDTO> getRowAcuse(final String periodo, final String cycle,final String tipo) throws PersistenceException {
        return executeSelectStatement(_coreSelectRowAcuse, periodo, cycle,tipo);
    }
}
