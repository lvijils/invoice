package hn.com.tigo.invoice.dto;

/**
 *
 * @author Leovijil
 */
public class InvoiceStatusModel {

    private String id;
    private String printedInvoiceStatus;
    private String printedInvoiceDetails;
    private String electronicInvoiceStatus;
    private String electronicInvoiceDetails;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrintedInvoiceStatus() {
        return printedInvoiceStatus;
    }

    public void setPrintedInvoiceStatus(String printedInvoiceStatus) {
        this.printedInvoiceStatus = printedInvoiceStatus;
    }

    public String getPrintedInvoiceDetails() {
        return printedInvoiceDetails;
    }

    public void setPrintedInvoiceDetails(String printedInvoiceDetails) {
        this.printedInvoiceDetails = printedInvoiceDetails;
    }

    public String getElectronicInvoiceStatus() {
        return electronicInvoiceStatus;
    }

    public void setElectronicInvoiceStatus(String electronicInvoiceStatus) {
        this.electronicInvoiceStatus = electronicInvoiceStatus;
    }

    public String getElectronicInvoiceDetails() {
        return electronicInvoiceDetails;
    }

    public void setElectronicInvoiceDetails(String electronicInvoiceDetails) {
        this.electronicInvoiceDetails = electronicInvoiceDetails;
    }

}
