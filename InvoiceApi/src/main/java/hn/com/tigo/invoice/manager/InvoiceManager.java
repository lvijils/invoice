package hn.com.tigo.invoice.manager;

import hn.com.tigo.invoice.dto.AcuseBycicloPerClienteDTO;
import hn.com.tigo.invoice.dto.AcuseAllMensajerosDTO;
import hn.com.tigo.invoice.dto.AcuseAllMotivosDTO;
import hn.com.tigo.invoice.dto.AcuseAllPerCiclosDTO;
import hn.com.tigo.invoice.dto.AllStatsDTO;
import hn.com.tigo.invoice.dto.AcuseAllStatsDTO;
import hn.com.tigo.invoice.dto.AllstatsMensByPerCicloDTO;
import hn.com.tigo.invoice.dto.InvoiceList;
import hn.com.tigo.invoice.dto.InvoiceStats;
import hn.com.tigo.invoice.dto.AcuseListDTO;
import hn.com.tigo.invoice.dto.AcuseRowDTO;
import hn.com.tigo.invoice.entity.AcuseByCicloPerClienteEntity;
import hn.com.tigo.invoice.entity.AcuseListMensajerosEntity;
import hn.com.tigo.invoice.entity.AcuseListMotivosEntity;
import hn.com.tigo.invoice.entity.AcuseStatsMensByPerCicloEntity;
import hn.com.tigo.invoice.entity.AllAcusesByPerCicloEntity;
import hn.com.tigo.invoice.entity.AllPerCiclosEntity;
import hn.com.tigo.invoice.entity.AllStatsAcusesEntity;
import hn.com.tigo.invoice.entity.AllStatsEntity;
import java.util.List;

import javax.sql.DataSource;

import hn.com.tigo.invoice.entity.InvoiceListEntity;
import hn.com.tigo.invoice.entity.InvoiceListParamEntity;
import hn.com.tigo.invoice.entity.InvoiceStatsEntity;
import hn.com.tigo.invoice.entity.AcusesParamEntity;
import hn.com.tigo.invoice.entity.RowAcusesListEntity;
import hn.com.tigo.josm.persistence.core.SessionBase;
import hn.com.tigo.josm.persistence.exception.PersistenceException;

/**
 * This class makes use of the DTO call that comes from the database.
 *
 * @author Leonardo Vijil
 * @version 1.0.0
 * @see
 * @since 12-17-2019 04:33:45 PM 2018
 */
public class InvoiceManager extends SessionBase {

    public InvoiceManager(final DataSource dataSource) throws PersistenceException {
        super(dataSource);
    }

    public String getInvoice(final String subscriber, final String cycle) throws PersistenceException {
        InvoiceListEntity entity = null;
        try {
            entity = new InvoiceListEntity(this);
            List<InvoiceList> response = entity.getInvoice(subscriber, cycle);
            if (response.isEmpty()) {
                return null;
            }
            return response.get(0).getInvoicebase64();
        } catch (PersistenceException e) {
            throw new PersistenceException(e.getMessage());

        }
    }

    public String getInvoiceB64(final String uuid) throws PersistenceException {
        InvoiceListEntity entity = null;
        try {
            entity = new InvoiceListEntity(this);
            List<InvoiceList> response = entity.getInvoiceB64(uuid);
            if (response.isEmpty()) {
                return null;
            }
            return response.get(0).getInvoicebase64();
        } catch (PersistenceException e) {
            throw new PersistenceException(e.getMessage());

        }
    }

    public List<InvoiceList> getInvoiceSubscriber(final String subscriber, final String current) throws PersistenceException {
        InvoiceListParamEntity entity = null;
        try {
            entity = new InvoiceListParamEntity(this);
            List<InvoiceList> response = entity.getInvoiceSubscriber(subscriber, current);
            return response;
        } catch (PersistenceException e) {
            throw new PersistenceException(e.getMessage());

        }
    }

    public List<InvoiceList> getInvoiceCustCode(final String custCode) throws PersistenceException {
        InvoiceListParamEntity entity = null;
        try {
            entity = new InvoiceListParamEntity(this);
            List<InvoiceList> response = entity.getInvoiceCustCode(custCode);
            return response;
        } catch (PersistenceException e) {
            throw new PersistenceException(e.getMessage());

        }
    }

    public List<InvoiceList> getInvoiceCustId(final String custId) throws PersistenceException {
        InvoiceListParamEntity entity = null;
        try {
            entity = new InvoiceListParamEntity(this);
            List<InvoiceList> response = entity.getInvoiceCustId(custId);
            return response;
        } catch (PersistenceException e) {
            throw new PersistenceException(e.getMessage());

        }
    }

    public List<InvoiceList> getInvoiceCustCodeAndBillCycle(final String custCode, final String billCycle) throws PersistenceException {
        InvoiceListParamEntity entity = null;
        try {
            entity = new InvoiceListParamEntity(this);
            List<InvoiceList> response = entity.getInvoiceCustCodeAndBillCycle(custCode, billCycle);
            return response;
        } catch (PersistenceException e) {
            throw new PersistenceException(e.getMessage());

        }
    }

    public List<InvoiceList> getInvoiceCustIdAndBillCycle(final String custId, final String billCycle) throws PersistenceException {
        InvoiceListParamEntity entity = null;
        try {
            entity = new InvoiceListParamEntity(this);
            List<InvoiceList> response = entity.getInvoiceCustIdAndBillCycle(custId, billCycle);
            return response;
        } catch (PersistenceException e) {
            throw new PersistenceException(e.getMessage());

        }
    }

    public List<InvoiceStats> getStatsCycle(final String billCycle) throws PersistenceException {
        InvoiceStatsEntity entity = null;
        try {
            entity = new InvoiceStatsEntity(this);
            List<InvoiceStats> response = entity.getStatsCycle(billCycle);
            return response;
        } catch (PersistenceException e) {
            throw new PersistenceException(e.getMessage());

        }
    }

    public List<AllStatsDTO> getAllStats() throws PersistenceException {
        AllStatsEntity entity = null;
        try {
            entity = new AllStatsEntity(this);
            List<AllStatsDTO> response = entity.getAllStats();
            return response;
        } catch (PersistenceException e) {
            throw new PersistenceException(e.getMessage());

        }
    }

    public List<AcuseAllPerCiclosDTO> getAllPerCiclos() throws PersistenceException {
        AllPerCiclosEntity entity = null;
        try {
            entity = new AllPerCiclosEntity(this);
            List<AcuseAllPerCiclosDTO> response = entity.getAllPerCiclos();
            return response;
        } catch (PersistenceException e) {
            throw new PersistenceException(e.getMessage());

        }
    }

    public List<AcuseAllMensajerosDTO> getAllMensajeros() throws PersistenceException {
        AcuseListMensajerosEntity entity = null;
        try {
            entity = new AcuseListMensajerosEntity(this);
            List<AcuseAllMensajerosDTO> response = entity.getAllMensajeros();
            return response;
        } catch (PersistenceException e) {
            throw new PersistenceException(e.getMessage());

        }
    }

    public List<AcuseAllMotivosDTO> getAllMotivos() throws PersistenceException {
        AcuseListMotivosEntity entity = null;
        try {
            entity = new AcuseListMotivosEntity(this);
            List<AcuseAllMotivosDTO> response = entity.getAllMotivos();
            return response;
        } catch (PersistenceException e) {
            throw new PersistenceException(e.getMessage());

        }
    }

    public List<AcuseAllStatsDTO> getAllStatsAcuses(final String periodo, final String ciclo) throws PersistenceException {
        AllStatsAcusesEntity entity = null;
        try {
            entity = new AllStatsAcusesEntity(this);
            List<AcuseAllStatsDTO> response = entity.getAllStatasAcuses(periodo, ciclo);
            return response;
        } catch (PersistenceException e) {
            throw new PersistenceException(e.getMessage());

        }
    }
    //getAllStatsMensajeros

    public List<AllstatsMensByPerCicloDTO> getAllStatsMensajeros(final String periodo, final String ciclo) throws PersistenceException {
        AcuseStatsMensByPerCicloEntity entity = null;
        try {
            entity = new AcuseStatsMensByPerCicloEntity(this);
            List<AllstatsMensByPerCicloDTO> response = entity.getAllStatsMensajeros(periodo, ciclo);
            return response;
        } catch (PersistenceException e) {
            throw new PersistenceException(e.getMessage());

        }
    }

    public List<AcuseListDTO> getAcuses(final String periodo, final String ciclo, final String tipo) throws PersistenceException {
        AllAcusesByPerCicloEntity entity = null;
        try {
            entity = new AllAcusesByPerCicloEntity(this);
            List<AcuseListDTO> response;
            if ("all".equals(tipo)) {
                response = entity.getAllAcuses(periodo, ciclo);
            } else {
                response = entity.getRowAcuses(periodo, ciclo, tipo);
            }
            return response;
        } catch (PersistenceException e) {
            throw new PersistenceException(e.getMessage());
        }
    }

    public List<AcuseRowDTO> getAllRowAcuses(final String periodo, final String ciclo, final String tipo) throws PersistenceException {
        RowAcusesListEntity entity = null;
        try {
            entity = new RowAcusesListEntity(this);
            List<AcuseRowDTO> response;
            if ("all".equals(tipo)) {
                response = entity.getAllAcuses(periodo, ciclo);
            } else {
                response = entity.getRowAcuse(periodo, ciclo, tipo);
            }
            return response;
        } catch (PersistenceException e) {
            throw new PersistenceException(e.getMessage());
        }
    }

    public List<AcuseBycicloPerClienteDTO> getExisteCliente(final String periodo, final String ciclo, final String cliente, final String id) throws PersistenceException {
        AcuseByCicloPerClienteEntity entity = null;
        try {
            entity = new AcuseByCicloPerClienteEntity(this);
            List<AcuseBycicloPerClienteDTO> response = entity.getExisteCliente(periodo, ciclo, cliente, id);
            return response;
        } catch (PersistenceException e) {
            throw new PersistenceException(e.getMessage());
        }
    }

    public void insertAcuseForm(final AcuseListDTO dto) throws PersistenceException {
        AcusesParamEntity entity = null;
        try {
            entity = new AcusesParamEntity(this);
            entity.insertAcuseForm(dto);
        } catch (PersistenceException e) {
            throw new PersistenceException(e.getMessage());
        }
    }

    public void updateAcuseForm(final AcuseListDTO dto) throws PersistenceException {
        AcusesParamEntity entity = null;
        try {
            entity = new AcusesParamEntity(this);
            entity.updateAcuseForm(dto);
        } catch (PersistenceException e) {
            throw new PersistenceException(e.getMessage());
        }
    }

    public void deleteAcuseForm(final AcuseListDTO dto) throws PersistenceException {
        AcusesParamEntity entity = null;
        try {
            entity = new AcusesParamEntity(this);
            entity.deleteAcuseForm(dto);
        } catch (PersistenceException e) {
            throw new PersistenceException(e.getMessage());
        }
    }
}
