package hn.com.tigo.invoice.dto;

public class AllstatsMensByPerCicloDTO extends DTO {

    private String nombre_mensajero;
    private long entregados;
    private long no_entregados;

    public String getNombre_mensajero() {
        return nombre_mensajero;
    }

    public void setNombre_mensajero(String nombre_mensajero) {
        this.nombre_mensajero = nombre_mensajero;
    }

    public long getEntregados() {
        return entregados;
    }

    public void setEntregados(long entregado) {
        this.entregados = entregado;
    }

    public long getNo_entregados() {
        return no_entregados;
    }

    public void setNo_entregados(long no_entregados) {
        this.no_entregados = no_entregados;
    }
}
