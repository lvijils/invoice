package hn.com.tigo.invoice.dto;

public class AcuseAllMotivosDTO extends DTO {

    private int id;
    private String nombre_motivo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre_motivo() {
        return nombre_motivo;
    }

    public void setNombre_motivo(String nombre_motivo) {
        this.nombre_motivo = nombre_motivo;
    }
}
