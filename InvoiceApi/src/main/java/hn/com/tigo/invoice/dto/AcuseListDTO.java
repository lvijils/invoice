package hn.com.tigo.invoice.dto;

public class AcuseListDTO extends DTO {

    private long id;
    private long idPeriodo;
    private long idCiclo;
    private String fecha;
    private String cliente;
    private long idMensajero;
    private long entregado;
    private long idMotivo;
    private String observacion;
    private String msgEntregado;

    public String getMsgEntregado() {
        return msgEntregado;
    }

    public void setMsgEntregado(String msgEntregado) {
        this.msgEntregado = msgEntregado;
    }

    public long getIdPeriodo() {
        return idPeriodo;
    }

    public void setIdPeriodo(long idPeriodo) {
        this.idPeriodo = idPeriodo;
    }

    public long getIdCiclo() {
        return idCiclo;
    }

    public void setIdCiclo(long idCiclo) {
        this.idCiclo = idCiclo;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public long getIdMotivo() {
        return idMotivo;
    }

    public void setIdMotivo(long idMotivo) {
        this.idMotivo = idMotivo;
    }

    public long getIdMensajero() {
        return idMensajero;
    }

    public void setIdMensajero(long idMensajero) {
        this.idMensajero = idMensajero;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public long getEntregado() {
        return entregado;
    }

    public void setEntregado(long entregado) {
        this.entregado = entregado;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
