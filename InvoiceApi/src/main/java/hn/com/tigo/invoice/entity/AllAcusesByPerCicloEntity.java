package hn.com.tigo.invoice.entity;

import hn.com.tigo.invoice.dto.AcuseListDTO;
import hn.com.tigo.josm.persistence.core.EntityBase;
import hn.com.tigo.josm.persistence.core.SessionBase;
import hn.com.tigo.josm.persistence.exception.PersistenceError;
import hn.com.tigo.josm.persistence.exception.PersistenceException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AllAcusesByPerCicloEntity extends EntityBase<AcuseListDTO> {

    private final String _coreSelectAllAcusesPerCiclo;
    private final String _coreSelectRowAcusesPerCiclo;

    public AllAcusesByPerCicloEntity(final SessionBase sessionBase) throws PersistenceException {
        super(sessionBase);
        _coreSelectAllAcusesPerCiclo = "SELECT id,id_periodo,id_ciclo,cliente,id_motivo,id_mensajero,observacion,fecha,entregado FROM REGISTRO_ACUSES WHERE id_periodo=? AND id_ciclo=?";
        _coreSelectRowAcusesPerCiclo = "SELECT id,id_periodo,id_ciclo,cliente,id_motivo,id_mensajero,observacion,fecha,entregado FROM REGISTRO_ACUSES WHERE id_periodo=? AND id_ciclo=? AND id=?";
    }

    @Override
    protected List<AcuseListDTO> fillList(ResultSet rs) throws PersistenceException {
        AcuseListDTO dto;
        ArrayList<AcuseListDTO> lista = new ArrayList<AcuseListDTO>();
        dto = new AcuseListDTO();
        try {
            while (rs.next()) {
                dto = new AcuseListDTO();
                dto.setId(rs.getLong("id"));
                dto.setIdPeriodo(rs.getLong("id_periodo"));
                dto.setIdCiclo(rs.getLong("id_ciclo"));
                dto.setFecha(rs.getString("fecha"));
                dto.setCliente(rs.getString("cliente"));
                dto.setIdMensajero(rs.getLong("id_mensajero"));
                dto.setEntregado(rs.getLong("entregado"));
                dto.setIdMotivo(rs.getLong("id_motivo"));
                dto.setObservacion(rs.getString("observacion"));
                lista.add(dto);
            }
            return lista;
        } catch (SQLException e) {
            throw new PersistenceException(PersistenceError.SQL, e);
        }

    }

    public final List<AcuseListDTO> getAllAcuses(final String periodo, final String cycle) throws PersistenceException {
        return executeSelectStatement(_coreSelectAllAcusesPerCiclo, periodo, cycle);
    }

    // getRowAcuses
    public final List<AcuseListDTO> getRowAcuses(final String periodo, final String cycle, final String id) throws PersistenceException {
        return executeSelectStatement(_coreSelectRowAcusesPerCiclo, periodo, cycle, id);
    }
}
