package hn.com.tigo.invoice.entity;

import hn.com.tigo.invoice.dto.AcuseListDTO;
import hn.com.tigo.josm.persistence.core.EntityBase;
import hn.com.tigo.josm.persistence.core.SessionBase;
import hn.com.tigo.josm.persistence.exception.PersistenceException;
import java.sql.ResultSet;
import java.util.List;

public class AcusesParamEntity extends EntityBase<AcuseListDTO> {

    private final String _coreInsert;
    private final String _coreUpdate;
    private final String _coreDelete;

    public AcusesParamEntity(final SessionBase sessionBase) throws PersistenceException { // TO_DATE('2020-04-13 21:17:56', 'YYYY-MM-DD HH24:MI:SS')
        super(sessionBase);
        _coreInsert = "INSERT INTO REGISTRO_ACUSES(id_periodo,id_ciclo,cliente,id_motivo,id_mensajero,observacion,fecha,entregado) VALUES(?,?,?,?,?,?,?,?)";
        _coreUpdate = "UPDATE REGISTRO_ACUSES SET id_periodo=?,id_ciclo=?,cliente=?,id_motivo=?,id_mensajero=?,observacion=?,fecha=?,entregado=? WHERE id=?";
        _coreDelete = "DELETE FROM REGISTRO_ACUSES WHERE id=?";
    }

    @Override
    protected List<AcuseListDTO> fillList(ResultSet rs) throws PersistenceException {
        return null;
    }

    public final void insertAcuseForm(final AcuseListDTO dto) throws PersistenceException {
        executeInsertStatement(_coreInsert, dto.getIdPeriodo(), dto.getIdCiclo(), dto.getCliente(), dto.getIdMotivo(), dto.getIdMensajero(), dto.getObservacion(), dto.getFecha(), dto.getEntregado());
    }

    public final void updateAcuseForm(final AcuseListDTO dto) throws PersistenceException {
        executeUpdateStatement(_coreUpdate, dto.getIdPeriodo(), dto.getIdCiclo(), dto.getCliente(), dto.getIdMotivo(), dto.getIdMensajero(), dto.getObservacion(), dto.getFecha(), dto.getEntregado(), dto.getId());
    }

    public final void deleteAcuseForm(final AcuseListDTO dto) throws PersistenceException {
        executeDeleteStatement(_coreDelete, dto.getId());
    }
}
